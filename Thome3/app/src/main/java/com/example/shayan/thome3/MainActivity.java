
package com.example.shayan.thome3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import static android.R.id.text1;

public class MainActivity extends BaseActivity{
    Button buttonName;
    TextView shoar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_3_1);

        bindView();

        buttonName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                shoar.setText(getString(R.string.text1));
                Toast.makeText(mContext, "سلام ایران!", Toast.LENGTH_SHORT).show();
            }
        });




    }

    private void bindView() {
        shoar=(TextView)findViewById(R.id.shoar);
        buttonName=(Button)findViewById(R.id.buttonName);
    }


}
