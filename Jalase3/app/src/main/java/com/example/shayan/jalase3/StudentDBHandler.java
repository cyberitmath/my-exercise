package com.example.shayan.jalase3;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by shayan on 10/30/2017.
 */

public class StudentDBHandler extends SQLiteOpenHelper {
    String tbQuery=""+
            "CREATE TABLE students( "+
            "_id integer primary key autoincrement , "+
            "name TEXT "+"family TEXT "+" )";

    public StudentDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(tbQuery);
    }
    public void insertStudent(String name,String family){
        String insertQuery="INSERT INTO student(name,family)"+"VALUES('"+name+"','"+family+"' )";
        SQLiteDatabase db=this.getWritableDatabase();
        db.execSQL(insertQuery);
        db.close();
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

    }
}
