package com.example.shayan.jalase3;

import android.content.Context;
import android.preference.PreferenceManager;

/**
 * Created by shayan on 10/18/2017.
 */

public class Publics {

    public static void setShared(Context mContext, String key, String value){
        PreferenceManager.getDefaultSharedPreferences(mContext).edit()
                .putString(key, value).apply();

    }
    public static String getShared(Context mContext,String key, String de_value){
        return PreferenceManager.getDefaultSharedPreferences(mContext).getString(key, de_value);

    }


}
