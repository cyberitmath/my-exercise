package com.example.shayan.jalase3.models;

/**
 * Created by shayan on 10/28/2017.
 */

public class Student {
    String name;
    String family;
    int age;
    String city;
    public String getName(){
        return name;
    }
    public void setName(){
    this.name=name;
    }

    public String getFamily(){
        return family;
    }
    public void setFamily(){
        this.family=family;
    }
    public int getAge(){
        return age;
    }
    public void setAge(){
        this.age=age;
    }
    public String getCity(){
        return city;
    }
    public void setCity(){
        this.city=city;
    }

}
