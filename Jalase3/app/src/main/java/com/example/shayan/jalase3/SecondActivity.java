package com.example.shayan.jalase3;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by shayan on 10/18/2017.
 */

public class SecondActivity extends BaseActivity {
    TextView result;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        bindview();
        String res = getIntent().getStringExtra("name") + "  " +
                getIntent().getStringExtra("family") +"\n"+ " age is:" +
                getIntent().getIntExtra("age", 35);
        result.setText(res);
    }

    private void bindview() {
        result = (TextView) findViewById(R.id.result);
    }

    @Override
    //بک دکمه بستن
    public void onBackPressed() {
        super.onBackPressed();
       // showToast("لطفا نرو!");
    }
}
