package com.example.shayan.jalase3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Yas0Activity extends BaseActivity implements View.OnClickListener {
    Button page2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yas0);

        page2=(Button)findViewById(R.id.page2);
       page2.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.page2){
            Intent page2Intent=new Intent(mContext,YasActivity.class);
            startActivity(page2Intent);
        }
    }
}
