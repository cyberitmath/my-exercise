package com.example.shayan.jalase3.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.shayan.jalase3.R;
import com.squareup.picasso.Picasso;

/**
 * Created by shayan on 10/19/2017.
 */

public class StudentListAdapter extends BaseAdapter {
    Context mContext;
    String names[];
    String avatars[];

    public StudentListAdapter(Context mContext,String[] names,String[] avatars){
        this.mContext=mContext;
        this.names=names;
        this.avatars=avatars;
    }



    @Override
    public int getCount() {
        return names.length;
    }

    @Override
    public Object getItem(int position) {
        return names[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View rowView = LayoutInflater.from(mContext).inflate(R.layout.student_list_item, viewGroup, false);
        TextView studentName=(TextView)rowView.findViewById(R.id.student);
        ImageView avatar=(ImageView)rowView.findViewById(R.id.avatar);
        studentName.setText(names[position]);
        Picasso.with(mContext)
                .load(avatars[position])
                .into(avatar);


        return rowView;
    }
}
