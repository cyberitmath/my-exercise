package com.example.shayan.jalase3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.orhanobut.hawk.Hawk;
import com.squareup
        .picasso.Picasso;

public class HawkActiye extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        TextView hawk;
        ImageView img;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hawk_actiye);


        Hawk.init(mContext).build();
        Hawk.put("my_name","Maryam Shayan");
        hawk=(TextView)findViewById(R.id.hawk);
        img=(ImageView)findViewById(R.id.img);
        Picasso.with(mContext).load("https://www.irib.ir/assets/slider_images/20170906090925_3579.png").into(img);
        String res=Hawk.get("my_name");
        hawk.setText(res);

    }


}
