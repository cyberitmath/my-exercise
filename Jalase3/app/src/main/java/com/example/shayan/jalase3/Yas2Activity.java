package com.example.shayan.jalase3;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Yas2Activity extends BaseActivity implements View.OnClickListener {
    EditText number;
    Button page3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yas2);

        number=(EditText)findViewById(R.id.number);
        findViewById(R.id.call).setOnClickListener(this);
        page3=(Button)findViewById(R.id.page3);
        page3.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.call){
            Intent callintent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+number.getText().toString()));
            mContext.startActivity(callintent);
        }else if (view.getId()==R.id.page3){
            Intent page3Intent=new Intent(mContext,Yas3Activity.class);
            startActivity(page3Intent);
        }
    }
}
