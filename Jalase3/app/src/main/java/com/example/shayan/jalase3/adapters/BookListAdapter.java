package com.example.shayan.jalase3.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.shayan.jalase3.R;
import com.squareup.picasso.Picasso;

import static com.example.shayan.jalase3.R.id.avatar;

/**
 * Created by shayan on 10/25/2017.
 */

public class BookListAdapter extends BaseAdapter {
    Context mContext;
    String bookName[];
    String avatars[];
    public BookListAdapter(Context mContext,String[]bookName,String[] avatars){
        this.mContext=mContext;
        this.bookName=bookName;
        this.avatars=avatars;
    }
    @Override
    public int getCount() {
        return bookName.length;
    }

    @Override
    public Object getItem(int position) {
        return bookName[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View rowView = LayoutInflater.from(mContext).inflate(R.layout.book_list_item, viewGroup, false);
        TextView bName=(TextView)rowView.findViewById(R.id.bName);
        ImageView avatarr=(ImageView) rowView.findViewById(R.id.avatarr);

        bName.setText(bookName[position]);
        Picasso.with(mContext)
                .load(avatars[position])
                .into(avatarr);
        return rowView;
    }
}
