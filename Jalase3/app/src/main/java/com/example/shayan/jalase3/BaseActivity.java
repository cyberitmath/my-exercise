package com.example.shayan.jalase3;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

/**
 * Created by shayan on 10/18/2017.
 */

public class BaseActivity extends AppCompatActivity {
    Context mContext=this;
    Activity mActivity=this;
     public void showToast(String txt){
         Toast.makeText(mContext,txt , Toast.LENGTH_SHORT).show();

     }
}
