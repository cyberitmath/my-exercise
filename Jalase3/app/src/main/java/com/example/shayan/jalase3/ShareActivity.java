package com.example.shayan.jalase3;

import android.content.Intent;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class ShareActivity extends BaseActivity implements View.OnClickListener {
    TextView result;
    EditText name;
    EditText family;
    EditText number;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share);

        bindview();
    }

    private void bindview() {
        result = (TextView) findViewById(R.id.result);
        name = (EditText) findViewById(R.id.name);
        number = (EditText) findViewById(R.id.number);
        family = (EditText) findViewById(R.id.family);
        findViewById(R.id.show).setOnClickListener(this);
        findViewById(R.id.save).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId()==R.id.show){
            String res=Publics.getShared(mContext,"name","noname")+" "+
                    Publics.getShared(mContext,"family","nofamily");
            result.setText(res);

        }
        else if (view.getId()==R.id.save){
            Publics.setShared(mContext,"name",name.getText().toString());
            Publics.setShared(mContext,"family",family.getText().toString());

        }
        else if (view.getId()==R.id.call){
            Intent intent=new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+number.getText().toString()));
            mContext.startActivity(intent);
        }

    }

}



