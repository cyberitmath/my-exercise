package com.example.shayan.jalase3;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class Yas3Activity extends BaseActivity {
    ImageView photo1;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yas3);

        bindViews();
        Picasso.with(mContext).load("https://www.irib.ir/assets/slider_images/20170906090925_3579.png").into(photo1);

    }

    private void bindViews() {
        photo1=(ImageView)findViewById(R.id.photo1);

    }


}
