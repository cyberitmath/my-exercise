package com.example.shayan.jalase3;

import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class YasActivity extends BaseActivity implements View.OnClickListener {
    TextView rezome;
    EditText name;
    EditText familyName;
    EditText email;
    EditText caseEdjucation;
    Button show;
    Button save;
    Button page2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yas);
        bindView();
    }

    private void bindView() {
        rezome = (TextView) findViewById(R.id.rezome);
        name = (EditText) findViewById(R.id.name);
        familyName = (EditText) findViewById(R.id.familyName);
        email = (EditText) findViewById(R.id.email);
        caseEdjucation = (EditText) findViewById(R.id.caseEdjucation);
        findViewById(R.id.show).setOnClickListener(this);
        findViewById(R.id.save).setOnClickListener(this);
        findViewById(R.id.page2).setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.show) {
            String res = getShared("name", "No name") + " " +
                    getShared("familyName", "no family") + " "+
                    getShared("caseEdjucation", "no caseEdjucation") +
                    getShared("email", "no email") + "\n";

            rezome.setText(res);
        } else if (view.getId() == R.id.save) {
            setShared("name", name.getText().toString());
            setShared("familyName", familyName.getText().toString());
            setShared("email", email.getText().toString());
            setShared("caseEdjucation", caseEdjucation.getText().toString());

        }else if (view.getId()==R.id.page2){
            Intent pageIntent=new Intent(mContext,Yas2Activity.class);
            startActivity(pageIntent);
        }

    }

    void setShared(String key, String value) {
        PreferenceManager.getDefaultSharedPreferences(mContext).edit().putString(key, value).apply();
    }

    String getShared(String key, String def_value) {
        return PreferenceManager.getDefaultSharedPreferences(mContext).getString(key, def_value);
    }
}
