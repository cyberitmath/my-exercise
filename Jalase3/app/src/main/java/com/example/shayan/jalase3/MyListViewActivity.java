package com.example.shayan.jalase3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.shayan.jalase3.adapters.StudentListAdapter;

public class MyListViewActivity extends BaseActivity {
    ListView myList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_list_view);
        myList=(ListView)findViewById(R.id.myListView);

        String names[]={
                "mina",
                "yas",
                "bahar",
                "selena"
        };
        String avatars[]={
                "https://www.irib.ir/assets/slider_images/20170906090925_3579.png",
                "https://www.irib.ir/assets/slider_images/20170906090925_3579.png",
                "https://www.irib.ir/assets/slider_images/20170906090925_3579.png",
                "https://www.irib.ir/assets/slider_images/20170906090925_3579.png"

        };


        StudentListAdapter adapter=new StudentListAdapter(mContext,names,avatars);
        myList.setAdapter(adapter);
        myList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                String clickedItem=(String)adapterView.getItemAtPosition(position);
                showToast(clickedItem);

            }
        });
    }

}
