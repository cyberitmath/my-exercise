package com.example.shayan.jalase3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class StudentDBActivity extends BaseActivity {
    EditText name;
    EditText family;
    StudentDBHandler dbHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_db);

        bindView();
    }

    private void bindView() {
        name=(EditText)findViewById(R.id.name);
        family=(EditText)findViewById(R.id.family);
        dbHandler=new StudentDBHandler(this,"sematec",null,1);
        findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dbHandler.insertStudent(name.getText().toString(), family.getText().toString());
                name.setText("");
                family.setText("");
                Toast.makeText(StudentDBActivity.this, "data has been save!", Toast.LENGTH_SHORT).show();

            }
        });
    }
}
