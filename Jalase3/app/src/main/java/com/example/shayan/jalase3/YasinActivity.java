package com.example.shayan.jalase3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.example.shayan.jalase3.adapters.BookListAdapter;

public class YasinActivity extends BaseActivity {
    ListView myList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_yasin);
        myList=(ListView)findViewById(R.id.myList);
        String bookName[]={
               "نام کتاب:خاطرات ثریا پهلوی"+"\n"+"\n"+"نویسنده:ثریا اسفندیاری بختیاری"+"\n"+"قیمت:1700 تومان",
                "نام کتاب:اعتماد به نفس برای همه"+"\n"+"\n"+"ناشر:کهکشان دانش"+"\n"+"قیمت:رایگان",
                "نام کتاب:تکنیک های یادگیری"+"\n"+"\n"+"ناشر:تجسم خلاق"+"\n"+"قیمت:5300 تومان",
                "نام کتاب:مهارت های مطالعه"+"\n"+"\n"+"نویسنده:جلیل معماریانی"+"\n"+"قیمت:2000 تومان",
                "نام کتاب:خداحافظ ناامیدی"+"\n"+"\n"+"ناشر:افراز"+"\n"+"قیمت:رایگان",
                "نام کتاب:عادت افراد تاثیر گذار"+"\n"+"\n"+"نویسنده:استیون کاوی"+"\n"+"قیمت:1000 تومان",
                "نام کتاب:خاطرات ثریا پهلوی"+"\n"+"\n"+"نویسنده:ثریا اسفندیاری بختیاری"+"\n"+"قیمت:1700 تومان",
                "نام کتاب:اعتماد به نفس برای همه"+"\n"+"\n"+"ناشر:کهکشان دانش"+"\n"+"قیمت:رایگان",
                "نام کتاب:تکنیک های یادگیری"+"\n"+"\n"+"ناشر:تجسم خلاق"+"\n"+"قیمت:5300 تومان"

        };
        String avatars[]={
            "http://file1.cdn.faraketab.ir/large_cover/9998.jpg",
            "http://file1.cdn.faraketab.ir/large_cover/10813.jpg",
            "http://file1.cdn.faraketab.ir/large_cover/11361.jpg",
            "http://file1.cdn.faraketab.ir/large_cover/871.jpg",
            "http://file1.cdn.faraketab.ir/large_cover/11277.jpg",
            "http://file1.cdn.faraketab.ir/large_cover/14350.jpg",
            "http://file1.cdn.faraketab.ir/large_cover/9998.jpg",
            "http://file1.cdn.faraketab.ir/large_cover/10813.jpg",
            "http://file1.cdn.faraketab.ir/large_cover/11361.jpg"

        };
        BookListAdapter adapter=new BookListAdapter(mContext,bookName,avatars);
        myList.setAdapter(adapter);
    }
}
