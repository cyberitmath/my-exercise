package com.example.shayan.jalase3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends BaseActivity implements View.OnClickListener {
    TextView txtView;
    Button button;
    Button startSecondActivity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindview();


    }

    private void bindview() {
        startSecondActivity = (Button) findViewById(R.id.startSecondActivity);
        startSecondActivity.setOnClickListener(this);

        txtView = (TextView) findViewById(R.id.textView);
        button = (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                txtView.setText(getString(R.string.editText));
                Toast.makeText(mContext, "سلام به سرزمینم ایران!", Toast.LENGTH_SHORT).show();
            }
        });


    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.startSecondActivity) {
            Intent secondActivityIntent = new Intent(mContext, SecondActivity.class);
            secondActivityIntent.putExtra("name", "Maryam");
            secondActivityIntent.putExtra("family", "PartoviShayan");
            secondActivityIntent.putExtra("age", "40");
            startActivity(secondActivityIntent);


        }


    }
}
;