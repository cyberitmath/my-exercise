package com.example.shayan.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    TextView studentData;
    EditText studentName;
    EditText studentFamily;
    EditText studentClass;
    Button sumButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tamrin_1);

        studentData = (TextView) findViewById(R.id.studentData);
        studentName = (EditText) findViewById(R.id.studentName);
        studentFamily = (EditText) findViewById(R.id.studentFamily);
        studentClass = (EditText) findViewById(R.id.studentClass);
        sumButton = (Button) findViewById(R.id.sumButton);

        sumButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                studentData.setText(studentName.getText().toString()
                        +" "+ studentFamily.getText().toString()
                        +"\n"+ studentClass.getText().toString());
            }
        });


    }
}
