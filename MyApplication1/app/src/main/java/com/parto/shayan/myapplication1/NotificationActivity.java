package com.parto.shayan.myapplication1;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class NotificationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        findViewById(R.id.send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendNotification();
            }
        });
    }
    void sendNotification(){
        Intent intentPause=new Intent("music_pause");
        PendingIntent pIntentPause = PendingIntent.getBroadcast(this, (int) System.currentTimeMillis(), intentPause, 0);


        Intent intentActivity = new Intent(this, VideoPlayerActivity.class);
        intentActivity.putExtra("key" , "value") ;
        PendingIntent pIntentActivity = PendingIntent.getActivity(this, (int) System.currentTimeMillis(), intentActivity, 0);

        Notification notif = new NotificationCompat.Builder(this)
                .setContentTitle("New mail from " + "test@gmail.com")
                .setContentText("Subject")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pIntentActivity)
                .setAutoCancel(true)
                .addAction(R.mipmap.ic_launcher, "pause", pIntentPause)
                .addAction(R.mipmap.ic_launcher, "More", pIntentPause)
                .addAction(R.mipmap.ic_launcher, "Remove", pIntentPause).build();

        NotificationManager notificationCompatManager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationCompatManager.notify(0, notif);


    }





}
