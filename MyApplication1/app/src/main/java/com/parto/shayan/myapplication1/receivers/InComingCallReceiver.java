package com.parto.shayan.myapplication1.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by shayan on 11/5/2017.
 */

public class InComingCallReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "InCominggggg Call!", Toast.LENGTH_SHORT).show();

    }
}
