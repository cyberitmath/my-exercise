package com.parto.shayan.myapplication1.Fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.parto.shayan.myapplication1.R;

/**
 * Created by shayan on 11/14/2017.
 */

public class FragmentC extends Fragment implements View.OnClickListener {
    public static FragmentC fragment;
    public static FragmentC getInstance(){
        if (null==fragment)
            fragment=new FragmentC();
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_c_layout, container, false);
        TextView txt1=(TextView)v.findViewById(R.id.txt1);
        //txt1.setText();
        txt1.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(getActivity(), "btn in fragment3", Toast.LENGTH_SHORT).show();
    }
}
