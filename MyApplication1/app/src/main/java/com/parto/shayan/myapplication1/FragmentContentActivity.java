package com.parto.shayan.myapplication1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class FragmentContentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_content);
    }
}
