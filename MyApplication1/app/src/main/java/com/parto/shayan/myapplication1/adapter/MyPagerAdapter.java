package com.parto.shayan.myapplication1.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.parto.shayan.myapplication1.Fragment.FragmentA;
import com.parto.shayan.myapplication1.Fragment.FragmentB;
import com.parto.shayan.myapplication1.Fragment.FragmentC;
import com.parto.shayan.myapplication1.Fragment.FragmentD;

/**
 * Created by shayan on 11/14/2017.
 */

public class MyPagerAdapter extends FragmentPagerAdapter {
    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position==0)
            return FragmentA.getInstance();
        if (position==1)
            return FragmentB.getInstance();
        if (position==2)
            return FragmentC.getInstance();
        if (position==3)
            return FragmentD.getInstance();
        return null;
    }

    @Override
    public int getCount() {
        return 0;
    }
}
