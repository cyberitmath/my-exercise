package com.parto.shayan.myapplication1;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.MediaController;
import android.widget.VideoView;

public class VideoPlayerActivity extends AppCompatActivity {
    VideoView myVideo;
    String videoURL = "https://as5.asset.aparat.com/aparat-video/8b34d697a115211afc65f59495a9420f8587783-144p__89298.mp4";
    BroadcastReceiver callReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_player);
        bindViews();
        if (ContextCompat.checkSelfPermission(VideoPlayerActivity.this,
                Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(VideoPlayerActivity.this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    1500);
        }

        myVideo.setMediaController(new MediaController(this));
        myVideo.setVideoURI(Uri.parse(videoURL));
        myVideo.start();
        callReceiver=new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (myVideo.isPlaying())
                    myVideo.pause();
            }
        };
        IntentFilter callFilter=new IntentFilter("android.intent.action.PHONE_STATE");
        registerReceiver(callReceiver,callFilter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(callReceiver
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 1500) {
            //read phone stated checkd!
        }
    }

    private void bindViews() {
        myVideo = (VideoView) findViewById(R.id.myVidew);
    }
}
