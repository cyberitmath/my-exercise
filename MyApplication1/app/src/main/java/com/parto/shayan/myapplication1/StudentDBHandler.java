package com.parto.shayan.myapplication1;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by shayan on 10/31/2017.
 */

public class StudentDBHandler extends SQLiteOpenHelper {
    String tblQuery = ""
            + "CREATE TABLE students("
            + "_id INTEGER AUTO INCREMENT PRIMARY KEY ,"
            + "name TEXT ,"
            + "family TEXT"
            + ")";

    public StudentDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(tblQuery);
    }

    public void insertStudent(String name, String family) {
        String insertQuery = "INSERT INTO students(name,family)" + "VALUES('" + name + "','" + family + "')";
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL(insertQuery);
        db.close();

    }





    public String getValues() {
        String results = "";
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT name,family from students";
        Cursor cursor = db.rawQuery(selectQuery, null);
        while (cursor.moveToNext()) {
            results += cursor.getString(0) + cursor.getString(1) + "\n";
        }


        return results;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {

    }
}
