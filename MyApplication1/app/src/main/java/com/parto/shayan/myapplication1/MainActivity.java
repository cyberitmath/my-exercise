package com.parto.shayan.myapplication1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText name;
    EditText family;
    TextView results;
    StudentDBHandler dbHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        name=(EditText)findViewById(R.id.name);
        family=(EditText)findViewById(R.id.family);
        results=(TextView)findViewById(R.id.results);
        dbHandler=new StudentDBHandler(this,"sama",null,1);
        findViewById(R.id.save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dbHandler.insertStudent(name.getText().toString(),family.getText().toString());
                name.setText("");
                family.setText("");
                Toast.makeText(MainActivity.this, "data has been save!", Toast.LENGTH_SHORT).show();
                results.setText(dbHandler.getValues());

            }
        });

    }
}
